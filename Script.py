#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import urllib
import urllib2
import random
from StringIO import StringIO
from PIL import Image



# kod EPSG 
srsEPSG = 2178


fullExtent = {
	'xmin' : 7500318,
	'ymin' : 5788735,
	'xmax' : 7502677,
	'ymax' : 5789967
}

tileSizeY = 481
tileSizeX = 711

imageWidth = 1416
imageHeight = 956
# Katalog docelowy. Jeśli nie istnieje to zostanie utworzony
targetDir = "D:\\Geoportal"

url = "http://mapa.um.warszawa.pl/mapviewer/foi?"
querystring = "request=getfoi&version=1.0&bbox=${xmin}:${ymin}:${xmax}:${ymax}&width=${imageWidth}&height=${imageHeight}&theme=dane_wawa.NMZ_WUP_WODA&clickable=no&area=yes&dstsrid=2178&wholeimage=yes&renderlabels=no&tid=653_1461267&aw=no"
payload = ""
headers = {
    'cache-control': "no-cache",
    'Postman-Token': "d23cc8e3-423e-43a0-9d54-0cae251a4de9"
    }

retryTimes = 1
# Podszycie się pod przegladarkę internetowa
userAgentString = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36"


tilesCount = 0
fileName = 0
pixelSizeX = tileSizeX/float(imageWidth)
pixelSizeY = tileSizeY/float(imageHeight)
if not os.path.exists(targetDir):
    os.makedirs(targetDir)
for y in range(fullExtent['ymax'], fullExtent['ymin'], - tileSizeY):
	for x in range(fullExtent['xmin'], fullExtent['xmax'], tileSizeX):
		targetFile = open(targetDir + "\\" + str(x) + "_" + str(y) + ".png", "wb")
		targetWorldFile = open(targetDir + "\\" + str(x) + "_" + str(y) +".pgw", "w")
		fileName = fileName + 1 
		url_link =url + querystring.replace("${ymin}", str(y - tileSizeY)).replace("${xmin}", str(x)).replace("${xmax}", str(x + tileSizeX)).replace("${ymax}", str(y)).replace("${imageWidth}", str(imageWidth)).replace("${imageHeight}",str(imageHeight))
		print "URL: " + url_link + "\n"
		textFile = str(pixelSizeX) + "\n" + str(0.0000000) + "\n" + str(0.0000000)+ "\n" + str(-pixelSizeY) + "\n" + str(x) + "\n" + str(y)

		#print textFile
		for tryNum in range(0, retryTimes):
			try: 
				response_get = urllib2.Request(url_link, None, {'User-Agent': userAgentString})
				response = urllib2.urlopen(response_get)
				responseURL = response.read().split(',')[5][7:].replace('"','')
				responseURL = responseURL.split('?refresh')[0]
				print responseURL

				
				req2 = urllib2.Request(responseURL, None, {'User-Agent': userAgentString})
				response2 = urllib2.urlopen(req2)
				responseText = response2.read()
				targetFile.write(responseText)
				targetWorldFile.write(textFile)
				#targetFile.close()
			except Exception:
				#img = Image.new('RGBA', (11328, 7648))
				#img.save(targetFile)
				#targetWorldFile.write(textFile)
				#targetFile.close()
				pass
