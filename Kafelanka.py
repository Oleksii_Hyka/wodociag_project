# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '12.27.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!
import sys
import webbrowser
import string
import os



import future
import time
import urllib.request
import threading
import tkinter.filedialog as filedialog
import tkinter

from PIL import Image
import signal
from PyQt5 import QtCore, QtGui, QtWidgets 


fullExtent = {
    'xmin' : 75003188,
    'ymin' : 5788735,
    'xmax' : 7502677,
    'ymax' : 5789967
    }
imageWidth = 1416
imageHeight = 956
srsEPSG = 2178
tileSizeX = 711
tileSizeY = 481
stop_variable = 0

RenderLable = "no"
theme = {
    'Woda' : "dane_wawa.NMZ_WUP_WODA",
    'Gaz' : "dane_wawa.NMZ_WUP_GAZ", 
    'CO' : "dane_wawa.NMZ_WUP_CO",
    'Specjalne' : "dane_wawa.NMZ_WUP_SPECJALNA",
    'Telekomunikacja' : "dane_wawa.NMZ_WUP_TELEFON",
    'Energia' : "dane_wawa.NMZ_WUP_ENERGIA",
    'Kanalizacja' : "dane_wawa.NMZ_WUP_KANALIZACJA",
    }
targetDir = "D:\\Geoportal"
url = "http://mapa.um.warszawa.pl/mapviewer/foi?"
currentTheme =  "dane_wawa.NMZ_WUP_CO"
userAgentString = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36"
querystring = "request=getfoi&version=1.0&bbox=${xmin}:${ymin}:${xmax}:${ymax}&width=${imageWidth}&height=${imageHeight}&theme=${currentTheme}&clickable=no&area=yes&dstsrid=${srsEPSG}&wholeimage=yes&renderlabels=${RenderLable}&tid=653_1461267&aw=no"

payload = ""
headers = {
    'cache-control': "no-cache",
    'Postman-Token': "d23cc8e3-423e-43a0-9d54-0cae251a4de9"
    }

class Ui_MainWindow(object):
    
    def open_url(self,gis_url):
        webbrowser.open('akame.com', new=2)
    
    def function_clear(self, log_string):
        log_string = ""
        self.output_log.setText(log_string)
    
    def stop_function(self, stop_variable):
        stop_variable = True
    
    def select_dir(self):
        global targetDir
        root = tkinter.Tk()
        root.withdraw()
        local_dir = filedialog.askdirectory(parent=root,initialdir="/",title='Please select a directory')
        targetDir = local_dir
        print(targetDir)
        print(type(targetDir))
        #targetDir = targetDir.encode('utf8')
        print(type(targetDir))

    def checkbox_toggled(self):
        global RenderLable
        if self.render_label.isChecked():
            RenderLable = "yes"
            
        else:
            RenderLable = "no"
    
    
    
    def autosave(self):
        
        path = targetDir + "\\" + "save_settings.txt"
        save_file = open(path, "w")
        list_of_variable = str(imageHeight) + "\n" + str(imageWidth) + "\n" + str(srsEPSG) + "\n" + str(fullExtent['xmin']) + "\n" +str(fullExtent['ymin']) + "\n" +str(fullExtent['xmax']) + "\n" + str(fullExtent['ymax']) + "\n" +str(tileSizeX) + "\n" + str(tileSizeY) + "\n" +str(currentTheme) + "\n" + str(targetDir) + "\n" + str(RenderLable)  
             
        save_file.write(list_of_variable)
        save_file.close()
    

    def backup_settings(self):
        global imageHeight
        global imageWidth
        global srsEPSG
        global fullExtent
        global tileSizeX
        global tileSizeY
        global currentTheme
        global targetDir
        global RenderLable
        tmp = ""
        backup_file = open(targetDir + "\\" + "save_settings.txt", "r")
        imageHeight = backup_file.readlines(0)
        imageWidth = backup_file.readlines(1)
        srsEPSG = backup_file.readlines(2)
        fullExtent['xmin'] = backup_file.readlines(3)
        fullExtent['ymin'] = backup_file.readlines(4)
        fullExtent['xmax'] = backup_file.readlines(5)
        fullExtent['ymax'] = backup_file.readlines(6)
        tileSizeX = backup_file.readlines(7)
        tileSizeY = backup_file.readlines(8)
        currentTheme = backup_file.readlines(9)
        targetDir = backup_file.readlines(10)
        RenderLable = backup_file.readlines(11)
        backup_file.close()
        
    def MainLoop(self):
        global log_string
        tilesCount = 0
        pixelSizeX = tileSizeX/float(imageWidth)
        pixelSizeY = tileSizeY/float(imageHeight)
        retryTimes = 1
        global list_of_errors
        list_of_errors = []
        log_string = "start"
        for y in range(fullExtent['ymax'], fullExtent['ymin'], - tileSizeY):
            for x in range(fullExtent['xmin'], fullExtent['xmax'], tileSizeX):
                url_link =url + querystring.replace("${ymin}", str('y - tileSizeY')).replace("${xmin}", str(x)).replace("${xmax}", str(x + tileSizeX)).replace("${ymax}", str(y)).replace("${imageWidth}", str(imageWidth)).replace("${imageHeight}",str(imageHeight)).replace("${currentTheme}", currentTheme).replace("${srsEPSG}", str(srsEPSG)).replace("${RenderLable}", str(RenderLable))
                textFile = str(pixelSizeX) + "\n" + str(0.0000000) + "\n" + str(0.0000000)+ "\n" + str(-pixelSizeY) + "\n" + str(x) + "\n" + str(y)#print textFile
                for tryNum in range(0, retryTimes):
                    
                        response = urllib.request.Request(url_link, None, {'User-Agent': userAgentString})
                        page = urllib.request.urlopen(response)
                        responseURL = page.read().split(',')[5][7:].replace('"','')
                        responseURL = responseURL.split('?refresh')[0]
                        print (responseURL)
                        
                        response2 = urllib.request.urlopen(responseURL, None, {'User-Agent': userAgentString})
                        responseText = response2.read()
                        targetFile = open(targetDir + "\\" + str(x) + "_" + str(y) + ".png", "wb")
                        targetWorldFile = open(targetDir + "\\" + str(x) + "_" + str(y) +".pgw", "w")
                        targetFile.write(responseText)
                        targetWorldFile.write(textFile)
                        log_string = str(log_string) + "\n" + str(responseURL)
                        
        
    def Change_variable(self):
        global imageHeight
        global imageWidth
        global srsEPSG
        global theme
        global fullExtent
        global tileSizeX
        global tileSizeY
        global currentTheme
        global stop_variable
        
        stop_variable = 1
        imageHeight = self.edit_imageHeight.text()
        imageWidth = self.edit_imageWidth.text()
        srsEPSG = str(self.list_srsEPSG.currentText())
        srsEPSG[5:10]
        currentTheme = theme[str(self.list_themes.currentText())]
        tileSizeX = int(self.edit_tileSizeX.text())
        tileSizeY = int(self.edit_tileSizeY.text())
        fullExtent['xmin'] = int(self.edit_xmin.text())
        fullExtent['ymin'] = int(self.edit_ymin.text())
        fullExtent['xmax'] = int(self.edit_xmax.text())
        fullExtent['ymax'] = int(self.edit_ymax.text())
        #self.autosave()
        #threading.Thread( self.MainLoop, ()).start()
        self.MainLoop()
        
        
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(680, 480)
        MainWindow.setStyleSheet("QDialog{\n"
"     background-color: rgb(18, 79, 170)\n"
"}\n"
"QPushButton{\n"
"   background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:1 rgba(255, 210, 115, 255))\n"
"}")    
        #self.backup_settings()
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(628, 338)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_13 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_13.setObjectName("verticalLayout_13")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setObjectName("label_7")
        self.verticalLayout_2.addWidget(self.label_7, 0, QtCore.Qt.AlignHCenter)
        self.edit_xmin = QtWidgets.QLineEdit(self.centralwidget)
        self.edit_xmin.setMinimumSize(QtCore.QSize(133, 0))
        self.edit_xmin.setText("")
        self.edit_xmin.setObjectName("edit_xmin")
        self.verticalLayout_2.addWidget(self.edit_xmin)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_4.addWidget(self.label_2, 0, QtCore.Qt.AlignHCenter)
        self.edit_ymin = QtWidgets.QLineEdit(self.centralwidget)
        self.edit_ymin.setMinimumSize(QtCore.QSize(133, 0))
        self.edit_ymin.setObjectName("edit_ymin")
        self.verticalLayout_4.addWidget(self.edit_ymin)
        self.verticalLayout_6 = QtWidgets.QVBoxLayout()
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.verticalLayout_6.addWidget(self.label_4, 0, QtCore.Qt.AlignHCenter)
        self.edit_imageWidth = QtWidgets.QLineEdit(self.centralwidget)
        self.edit_imageWidth.setMinimumSize(QtCore.QSize(133, 0))
        self.edit_imageWidth.setObjectName("edit_imageWidth")
        self.verticalLayout_6.addWidget(self.edit_imageWidth)
        self.verticalLayout_9 = QtWidgets.QVBoxLayout()
        self.verticalLayout_9.setObjectName("verticalLayout_9")
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setMinimumSize(QtCore.QSize(41, 0))
        self.label_10.setObjectName("label_10")
        self.verticalLayout_9.addWidget(self.label_10, 0, QtCore.Qt.AlignHCenter)
        self.edit_tileSizeX = QtWidgets.QLineEdit(self.centralwidget)
        self.edit_tileSizeX.setMinimumSize(QtCore.QSize(133, 0))
        self.edit_tileSizeX.setObjectName("edit_tileSizeX")
        self.verticalLayout_9.addWidget(self.edit_tileSizeX)
        self.verticalLayout_11 = QtWidgets.QVBoxLayout()
        self.verticalLayout_11.setObjectName("verticalLayout_11")
        self.label_12 = QtWidgets.QLabel(self.centralwidget)
        self.label_12.setMinimumSize(QtCore.QSize(37, 0))
        self.label_12.setObjectName("label_12")
        self.verticalLayout_11.addWidget(self.label_12, 0, QtCore.Qt.AlignHCenter)
        self.list_themes = QtWidgets.QComboBox(self.centralwidget)
        self.list_themes.setMinimumSize(QtCore.QSize(133, 0))
        self.list_themes.setObjectName("list_themes")
        self.list_themes.addItem("")
        self.list_themes.addItem("")
        self.list_themes.addItem("")
        self.list_themes.addItem("")
        self.list_themes.addItem("")
        self.list_themes.addItem("")
        self.list_themes.addItem("")
        self.verticalLayout_11.addWidget(self.list_themes)
        self.verticalLayout_9.addLayout(self.verticalLayout_11)
        self.verticalLayout_6.addLayout(self.verticalLayout_9)
        self.verticalLayout_4.addLayout(self.verticalLayout_6)
        self.verticalLayout_2.addLayout(self.verticalLayout_4)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setObjectName("label_9")
        self.verticalLayout_3.addWidget(self.label_9, 0, QtCore.Qt.AlignHCenter)
        self.edit_xmax = QtWidgets.QLineEdit(self.centralwidget)
        self.edit_xmax.setMinimumSize(QtCore.QSize(133, 0))
        self.edit_xmax.setObjectName("edit_xmax")
        self.verticalLayout_3.addWidget(self.edit_xmax)
        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setMinimumSize(QtCore.QSize(25, 0))
        self.label.setObjectName("label")
        self.verticalLayout_5.addWidget(self.label, 0, QtCore.Qt.AlignHCenter)
        self.edit_ymax = QtWidgets.QLineEdit(self.centralwidget)
        self.edit_ymax.setMinimumSize(QtCore.QSize(133, 0))
        self.edit_ymax.setObjectName("edit_ymax")
        self.verticalLayout_5.addWidget(self.edit_ymax)
        self.verticalLayout_7 = QtWidgets.QVBoxLayout()
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setMinimumSize(QtCore.QSize(59, 0))
        self.label_5.setObjectName("label_5")
        self.verticalLayout_7.addWidget(self.label_5, 0, QtCore.Qt.AlignHCenter)
        self.edit_imageHeight = QtWidgets.QLineEdit(self.centralwidget)
        self.edit_imageHeight.setMinimumSize(QtCore.QSize(133, 0))
        self.edit_imageHeight.setObjectName("edit_imageHeight")
        self.verticalLayout_7.addWidget(self.edit_imageHeight)
        self.verticalLayout_8 = QtWidgets.QVBoxLayout()
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.label_11 = QtWidgets.QLabel(self.centralwidget)
        self.label_11.setMinimumSize(QtCore.QSize(41, 0))
        self.label_11.setObjectName("label_11")
        self.verticalLayout_8.addWidget(self.label_11, 0, QtCore.Qt.AlignHCenter)
        self.edit_tileSizeY = QtWidgets.QLineEdit(self.centralwidget)
        self.edit_tileSizeY.setMinimumSize(QtCore.QSize(133, 0))
        self.edit_tileSizeY.setObjectName("edit_tileSizeY")
        self.verticalLayout_8.addWidget(self.edit_tileSizeY)
        self.verticalLayout_7.addLayout(self.verticalLayout_8)
        self.verticalLayout_12 = QtWidgets.QVBoxLayout()
        self.verticalLayout_12.setObjectName("verticalLayout_12")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setMinimumSize(QtCore.QSize(39, 0))
        self.label_3.setObjectName("label_3")
        self.verticalLayout_12.addWidget(self.label_3, 0, QtCore.Qt.AlignHCenter)
        self.list_srsEPSG = QtWidgets.QComboBox(self.centralwidget)
        self.list_srsEPSG.setMinimumSize(QtCore.QSize(133, 0))
        self.list_srsEPSG.setObjectName("list_srsEPSG")
        self.list_srsEPSG.addItem("")
        self.list_srsEPSG.addItem("")
        self.list_srsEPSG.addItem("")
        self.list_srsEPSG.addItem("")
        self.list_srsEPSG.addItem("")
        self.list_srsEPSG.addItem("")
        self.list_srsEPSG.addItem("")
        self.verticalLayout_12.addWidget(self.list_srsEPSG)
        self.verticalLayout_7.addLayout(self.verticalLayout_12)
        self.verticalLayout_5.addLayout(self.verticalLayout_7)
        self.verticalLayout_3.addLayout(self.verticalLayout_5)
        self.horizontalLayout.addLayout(self.verticalLayout_3)
        #####
        self.verticalLayout.addLayout(self.horizontalLayout)
        ###
        
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.render_label = QtWidgets.QCheckBox(self.centralwidget)
        self.render_label.setObjectName("render_label")
        self.horizontalLayout_8.addWidget(self.render_label)
        self.render_label.toggled.connect(self.checkbox_toggled)
        self.empty_tiles = QtWidgets.QCheckBox(self.centralwidget)
        self.empty_tiles.setObjectName("empty_tiles")
        self.horizontalLayout_8.addWidget(self.empty_tiles)
        self.verticalLayout.addLayout(self.horizontalLayout_8)
        #self.empty_tiles.toggled.connect(self.checkbox_tiles)
        ########################
        self.verticalLayout.addWidget(self.render_label, 0, QtCore.Qt.AlignHCenter)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout_3.addLayout(self.verticalLayout)
        self.verticalLayout_10 = QtWidgets.QVBoxLayout()
        self.verticalLayout_10.setObjectName("verticalLayout_10")
        self.output_log = QtWidgets.QTextEdit(self.centralwidget)
        self.output_log.setObjectName("output_log")
        self.verticalLayout_10.addWidget(self.output_log)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.SelectFolder = QtWidgets.QPushButton(self.centralwidget)
        self.SelectFolder.setMinimumSize(QtCore.QSize(75, 0))
        self.SelectFolder.setObjectName("SelectFolder")
        self.horizontalLayout_2.addWidget(self.SelectFolder)
        self.SelectFolder.clicked.connect(self.select_dir)
        ################
        self.start_script = QtWidgets.QPushButton(self.centralwidget)
        self.start_script.setMinimumSize(QtCore.QSize(75, 0))
        self.start_script.setObjectName("start_script")
        self.horizontalLayout_2.addWidget(self.start_script)
        self.start_script.clicked.connect(self.Change_variable)
        ###############
        self.stop_script = QtWidgets.QPushButton(self.centralwidget)
        self.stop_script.setMinimumSize(QtCore.QSize(75, 0))
        self.stop_script.setObjectName("stop_script")
        self.horizontalLayout_2.addWidget(self.stop_script)
        self.stop_script.clicked.connect(self.stop_function)
        #####################
        self.Clear_log = QtWidgets.QPushButton(self.centralwidget)
        self.Clear_log.setMinimumSize(QtCore.QSize(75, 0))
        self.Clear_log.setObjectName("Clear_log")
        self.horizontalLayout_2.addWidget(self.Clear_log)
        self.Clear_log.clicked.connect(self.function_clear)
        ########################
        self.verticalLayout_10.addLayout(self.horizontalLayout_2)
        self.commandLinkButton = QtWidgets.QCommandLinkButton(self.centralwidget)
        self.commandLinkButton.setMinimumSize(QtCore.QSize(320, 0))
        self.commandLinkButton.setObjectName("commandLinkButton")
        self.verticalLayout_10.addWidget(self.commandLinkButton)
        self.commandLinkButton.clicked.connect(self.open_url)
        #############################
        self.horizontalLayout_3.addLayout(self.verticalLayout_10)
        self.verticalLayout_13.addLayout(self.horizontalLayout_3)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 628, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        
        self.retranslateUi(MainWindow)
        self.list_srsEPSG.setCurrentIndex(4)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
    
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label_5.setText(_translate("MainWindow", "imageHeight"))
        self.render_label.setText(_translate("MainWindow", "renderLabel"))
        self.label_3.setText(_translate("MainWindow", "srsEPSG"))
        
        self.label_4.setText(_translate("MainWindow", "imageWidth"))
        self.commandLinkButton.setText(_translate("MainWindow", "Oleksii Hyka"))
        
        self.edit_xmin.setText(_translate("MainWindow", "7500318"))
        self.edit_xmax.setText(_translate("MainWindow", "7502677"))
        self.edit_ymin.setText(_translate("MainWindow", "5788735"))
        self.edit_ymax.setText(_translate("MainWindow", "5789967"))
        self.edit_imageWidth.setText(_translate("MainWindow", "1416"))
        self.edit_imageHeight.setText(_translate("MainWindow", "956"))
        
        self.edit_tileSizeX.setText(_translate("MainWindow", "711"))
        self.edit_tileSizeY.setText(_translate("MainWindow", "481"))
       
        self.label.setText(_translate("MainWindow", "Ymax"))
        self.label_2.setText(_translate("MainWindow", "Ymin"))
        self.label_7.setText(_translate("MainWindow", "Xmin"))
        self.label_9.setText(_translate("MainWindow", "Xmax"))
        self.list_srsEPSG.setItemText(4, _translate("MainWindow", "EPSG:2176"))
        self.list_srsEPSG.setItemText(1, _translate("MainWindow", "EPSG:2177"))
        self.list_srsEPSG.setItemText(2, _translate("MainWindow", "EPSG:2178"))
        self.list_srsEPSG.setItemText(3, _translate("MainWindow", "EPSG:2179"))
        self.list_srsEPSG.setItemText(0, _translate("MainWindow", "EPSG:2180"))
        self.list_srsEPSG.setItemText(5, _translate("MainWindow", "EPSG:3857"))
        self.list_srsEPSG.setItemText(6, _translate("MainWindow", "EPSG:4326"))
        self.label_10.setText(_translate("MainWindow", "TileSizeX"))
        self.label_11.setText(_translate("MainWindow", "TileSizeY"))
        self.list_themes.setItemText(0, _translate("MainWindow", "Woda"))
        self.list_themes.setItemText(1, _translate("MainWindow", "Kanalizacja"))
        self.list_themes.setItemText(2, _translate("MainWindow", "CO"))
        self.list_themes.setItemText(3, _translate("MainWindow", "Gaz"))
        self.list_themes.setItemText(4, _translate("MainWindow", "Specjalne"))
        self.list_themes.setItemText(5, _translate("MainWindow", "Energia"))
        self.list_themes.setItemText(6, _translate("MainWindow", "Telekomunikacja"))
        self.label_12.setText(_translate("MainWindow", "Themes"))
        self.Clear_log.setText(_translate("MainWindow", "Clear"))
        self.SelectFolder.setText(_translate("MainWindow", "SelectFolder"))
        self.start_script.setText(_translate("MainWindow", "Start"))
        self.stop_script.setText(_translate("MainWindow", "Stop"))
        self.empty_tiles.setText(_translate("MainWindow", "EmptyTiles"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())