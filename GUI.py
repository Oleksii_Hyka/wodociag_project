# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Warszawa.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(688, 523)
        self.textEdit_2 = QtWidgets.QTextEdit(Form)
        self.textEdit_2.setGeometry(QtCore.QRect(40, 250, 104, 31))
        self.textEdit_2.setObjectName("textEdit_2")
        self.label = QtWidgets.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(30, 170, 121, 16))
        self.label.setObjectName("label")
        self.textEdit_4 = QtWidgets.QTextEdit(Form)
        self.textEdit_4.setGeometry(QtCore.QRect(180, 190, 104, 31))
        self.textEdit_4.setObjectName("textEdit_4")
        self.label_4 = QtWidgets.QLabel(Form)
        self.label_4.setGeometry(QtCore.QRect(200, 170, 71, 16))
        self.label_4.setObjectName("label_4")
        self.label_3 = QtWidgets.QLabel(Form)
        self.label_3.setGeometry(QtCore.QRect(70, 290, 47, 13))
        self.label_3.setObjectName("label_3")
        self.label_2 = QtWidgets.QLabel(Form)
        self.label_2.setGeometry(QtCore.QRect(30, 230, 131, 20))
        self.label_2.setObjectName("label_2")
        self.textEdit = QtWidgets.QTextEdit(Form)
        self.textEdit.setGeometry(QtCore.QRect(40, 190, 104, 31))
        self.textEdit.setObjectName("textEdit")
        self.textEdit_6 = QtWidgets.QTextEdit(Form)
        self.textEdit_6.setGeometry(QtCore.QRect(180, 310, 104, 31))
        self.textEdit_6.setObjectName("textEdit_6")
        self.label_8 = QtWidgets.QLabel(Form)
        self.label_8.setGeometry(QtCore.QRect(220, 290, 47, 13))
        self.label_8.setObjectName("label_8")
        self.textEdit_3 = QtWidgets.QTextEdit(Form)
        self.textEdit_3.setGeometry(QtCore.QRect(40, 310, 104, 31))
        self.textEdit_3.setObjectName("textEdit_3")
        self.textEdit_5 = QtWidgets.QTextEdit(Form)
        self.textEdit_5.setGeometry(QtCore.QRect(180, 250, 104, 31))
        self.textEdit_5.setObjectName("textEdit_5")
        self.label_5 = QtWidgets.QLabel(Form)
        self.label_5.setGeometry(QtCore.QRect(200, 230, 101, 20))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(Form)
        self.label_6.setGeometry(QtCore.QRect(140, 350, 47, 13))
        self.label_6.setObjectName("label_6")
        self.mQgsProjectionSelectionWidget = QgsProjectionSelectionWidget(Form)
        self.mQgsProjectionSelectionWidget.setGeometry(QtCore.QRect(470, 410, 160, 27))
        self.mQgsProjectionSelectionWidget.setObjectName("mQgsProjectionSelectionWidget")
        self.mComboBox = QgsCheckableComboBox(Form)
        self.mComboBox.setGeometry(QtCore.QRect(90, 370, 131, 27))
        self.mComboBox.setObjectName("mComboBox")
        self.mComboBox.addItem("")
        self.mComboBox.addItem("")
        self.mComboBox.addItem("")
        self.mComboBox.addItem("")
        self.mComboBox.addItem("")
        self.mComboBox.addItem("")
        self.mComboBox.addItem("")
        self.buttonBox = QtWidgets.QDialogButtonBox(Form)
        self.buttonBox.setGeometry(QtCore.QRect(470, 460, 156, 23))
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.checkBox = QtWidgets.QCheckBox(Form)
        self.checkBox.setGeometry(QtCore.QRect(40, 410, 81, 17))
        self.checkBox.setObjectName("checkBox")
        self.checkBox_2 = QtWidgets.QCheckBox(Form)
        self.checkBox_2.setGeometry(QtCore.QRect(190, 410, 81, 17))
        self.checkBox_2.setObjectName("checkBox_2")
        self.commandLinkButton = QtWidgets.QCommandLinkButton(Form)
        self.commandLinkButton.setGeometry(QtCore.QRect(240, 450, 185, 41))
        self.commandLinkButton.setObjectName("commandLinkButton")
        self.OutputLine = QtWidgets.QListView(Form)
        self.OutputLine.setGeometry(QtCore.QRect(315, 170, 351, 192))
        self.OutputLine.setObjectName("OutputLine")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.label.setText(_translate("Form", "Lewe górne współrzędne"))
        self.label_4.setText(_translate("Form", "imageWidth"))
        self.label_3.setText(_translate("Form", "srsEPSG"))
        self.label_2.setText(_translate("Form", "Prawe golne współrzędne"))
        self.label_8.setText(_translate("Form", "TID"))
        self.label_5.setText(_translate("Form", "imageHeight"))
        self.label_6.setText(_translate("Form", "Themes"))
        self.mComboBox.setItemText(0, _translate("Form", "Woda"))
        self.mComboBox.setItemText(1, _translate("Form", "Kanalizacja"))
        self.mComboBox.setItemText(2, _translate("Form", "Energia"))
        self.mComboBox.setItemText(3, _translate("Form", "Telekomunikacja"))
        self.mComboBox.setItemText(4, _translate("Form", "Gaz"))
        self.mComboBox.setItemText(5, _translate("Form", "CO"))
        self.mComboBox.setItemText(6, _translate("Form", "Specjalne"))
        self.checkBox.setText(_translate("Form", "RenderLable"))
        self.checkBox_2.setText(_translate("Form", "Clickable"))
        self.commandLinkButton.setText(_translate("Form", "https://gis-expert.pl"))

from qgscheckablecombobox import QgsCheckableComboBox
from qgsprojectionselectionwidget import QgsProjectionSelectionWidget

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())

